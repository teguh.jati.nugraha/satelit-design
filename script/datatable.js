$(document).ready(function() {
    $('#payloadList').DataTable({
       'pageLength': 5,
       'lengthMenu' : [5, 10, 15, 20],
       'responsive' : true,
    });

    $('#commentList').DataTable({
       'pageLength': 5,
       'lengthMenu' : [5, 10, 15, 20],
       'responsive' : true,
    });
} );