$(document).ready(function() {

  // Set last page opened on the menu
  $('#menu a[href]').on('click', function() {
    sessionStorage.setItem('menu', $(this).attr('href'));
  });

  if (!sessionStorage.getItem('menu')) {
    $('#menu #dashboard').addClass('active');
  } else {
    // Sets active and open to selected page in the left column menu.
    $('#menu a[href=\'' + sessionStorage.getItem('menu') + '\']').parents('li').addClass('active open');
  }

  if (localStorage.getItem('column-left') == 'active') {
    // $('#button-menu i').replaceWith('<i class="fa fa-dedent fa-lg"></i>');
    $('#button-menu i').replaceWith('<i class="fa fa-bars fa-lg"></i>');

    $('#column-left').addClass('active');

    // Slide Down Menu
    $('#menu li.active').has('ul').children('ul').addClass('collapse in');
    $('#menu li').not('.active').has('ul').children('ul').addClass('collapse in');
  } else {
    $('#button-menu i').replaceWith('<i class="fa fa-bars fa-lg"></i>');

    $('#menu li li.active').has('ul').children('ul').addClass('collapse in');
    $('#menu li li').not('.active').has('ul').children('ul').addClass('collapse');

  }

  // Menu button
  $('#button-menu').on('click', function() {
    // Checks if the left column is active or not.
    if ($('#column-left').hasClass('active')) {
      localStorage.setItem('column-left', '');

      $('#button-menu i').replaceWith('<i class="fa fa-bars fa-lg"></i>');

      $('#column-left').removeClass('active');
      $('#profile .media-list .media2').removeClass('hidden');
      $('#profile .media-list .media').addClass('hidden');      

      $('#menu > li > ul').removeClass('in collapse');
      $('#menu > li > ul').removeAttr('style');
    } else {
      localStorage.setItem('column-left', 'active');

      $('#button-menu i').replaceWith('<i class="fa fa-bars fa-lg"></i>');

      $('#column-left').addClass('active');
      $('#profile .media-list .media').removeClass('hidden'); 
      $('#profile .media-list .media2').addClass('hidden');  

      // Add the slide down to open menu items
      $('#menu li.open').has('ul').children('ul').addClass('collapse in');
      $('#menu li').not('.open').has('ul').children('ul').addClass('collapse');
    }
  });

  // Menu
  $('#menu').find('li').has('ul').children('a').on('click', function() {
    if ($('#column-left').hasClass('active')) {
      $(this).parent('li').toggleClass('open').children('ul').collapse('toggle');
      // $(this).parent('li').siblings().removeClass('open').children('ul.in').collapse('hide');
    } else if (!$(this).parent().parent().is('#menu')) {
      $(this).parent('li').toggleClass('open').children('ul').collapse('toggle');
      // $(this).parent('li').siblings().removeClass('open').children('ul.in').collapse('hide');
    }
  });

  // $('#button-menu').click(function(){
  //     if(!$(this).find('#column-left').hasClass('active')){
  //         $(this).parent().parent().parent().parent().parent().parent().find('.media').addClass('hidden');
  //         $(this).parent().parent().parent().parent().parent().parent().find('.media2').removeClass('hidden');
  //     }else if(!$(this).find('#column-left:not([active])')){
  //         $(this).parent().parent().parent().parent().parent().parent().find('.media').removeClass('hidden');
  //         $(this).parent().parent().parent().parent().parent().parent().find('.media2').addClass('hidden');
  //     }
  // });

});