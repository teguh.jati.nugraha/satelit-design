$(document).ready(function() {
  
    $('#date').datepicker({
        format: 'dd/M/yyyy'
    }).on('change', function () {
        $('.datepicker').hide();
    });

    // $('input[name="upload-im2"]').hide();
    // $('input[name="upload-im3"]').hide();
    // $('img[name="im-2"]').hide();
    // $('img[name="im-3"]').hide();

    // //show it when the checkbox is clicked
    // $('input[name="checkbox-2"]').on('click', function () {
    //     if ($(this).prop('checked')) {
    //         $('img[name="im-2"]').fadeIn();
    //         $('input[name="upload-im2"]').fadeIn();
    //     } else {
    //         $('img[name="im-2"]').hide();
    //         $('input[name="upload-im2"]').hide();
    //     }
    // });

    // $('input[name="checkbox-2"]').on('click', function () {
    //     if ($(this).prop('checked')) {
    //         $('img[name="im-2"]').fadeIn();
    //         $('input[name="upload-im2"]').fadeIn();
    //     } else {
    //         $('img[name="im-2"]').hide();
    //         $('input[name="upload-im2"]').hide();
    //     }
    // });

      // Initialize scrollers.
      $('.demo2').simplebar();
      
      // Demonstration of changing the vertical scroller content and dimensions.
      $('.add-more').on('click', function(e){
        e.preventDefault();
        $('.demo1').simplebar('getContentElement').append('<p class="odd">Additional content 1</p><p>Additional content 2</p><p p class="odd">Additional content 3</p><p>Additional content 4</p>');
        $('.demo1').simplebar('recalculate');
      });
      $('.toggle-height').on('click', function(e){
        e.preventDefault();
        if ($('.demo1').height() === 400) {
          $('.demo1').height(300);
        } else {
          $('.demo1').height(400);
        }
        $('.demo1').simplebar('recalculate');
      });
      // Demonstration of changing the horizontal scroller content and dimensions.
      $('.add-more-horiz').on('click', function(e){
        e.preventDefault();
        $('.demo2').simplebar('getContentElement').append('<div class="box">A</div><div class="box">B</div><div class="box">C</div><div class="box">D</div>');
        $('.demo2').simplebar('recalculate');
      });
      $('.toggle-width').on('click', function(e){
        e.preventDefault();
        if ($('.demo2').width() === 600) {
          $('.demo2').width(500);
        } else {
          $('.demo2').width(600);
        }
        $('.demo2').simplebar('recalculate');
      });
      $('#hiddenInput2').hide();
      $('#hiddenInput3').hide();
      $('#hiddenInput4').hide();
      $('#hiddenInput5').hide();
      $('#checkbox-2').on('change', function() {
          $this = $(this);
          $input = $this.parent().parent().parent().parent().parent().parent().parent().find('#hiddenInput2');
          $input2 = $this.parent().parent().parent().parent().parent().parent().parent().find('#hiddenInput4');
          if($this.is(':checked')) {
              $input.slideDown();
              $input2.slideDown();
          } else {
              $input.slideUp();
              $input2.slideUp();
          }
      });
      $('#checkbox-3').on('change', function() {
          $this = $(this);
          $input = $this.parent().parent().parent().parent().parent().parent().parent().find('#hiddenInput3');
          $input2 = $this.parent().parent().parent().parent().parent().parent().parent().find('#hiddenInput5');
          if($this.is(':checked')) {
              $input.slideDown();
              $input2.slideDown();
          } else {
              $input.slideUp();
              $input2.slideUp();
          }
      });
});

function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#upload-im1')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
};
function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#upload-im2')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
};
function readURL3(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#upload-im3')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
};
function readURL4(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#upload-im4')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
};
function readURL5(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#upload-im5')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
};
function readURL6(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#upload-im6')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
};
// MUlTISELECT MENU PAYLOAD
$('#custom-headers').multiSelect({
  // selectableHeader: "<div class='custom-header'>Selectable items</div>",
  // selectionHeader: "<div class='custom-header'>Selection items</div>",
  // selectableFooter: "<div class='custom-header'>Selectable footer</div>",
  // selectionFooter: "<div class='custom-header'>Selection footer</div>"
});

$('#satSelect').on('change', function (e) {
    $('#myTab li a').eq($(this).val()).tab('show'); 
});
$('#custSelect').on('change', function (e) {
    $('#custTab li a').eq($(this).val()).tab('show'); 
});

